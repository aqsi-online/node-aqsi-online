/* eslint-disable max-classes-per-file */
const API_ERRORS = {
  401: 'Клиентский сертификат не прошел проверку',
  409: 'Чек с данным идентификатором уже был создан в системе',
  400: 'Некорректный запрос',
  503: 'Очередь документов переполнена, в ответе возвращается хидер Retry-After с таймаутом в секундах',
};

class AqsiOnlineError extends Error {
  constructor(message, errors) {
    super(message);
    this.name = 'AqsiOnlineError';
    this.errors = errors;
    Error.captureStackTrace(this, AqsiOnlineError);
  }
}

class AqsiOnlineValidationError extends AqsiOnlineError {
  constructor(errors) {
    const flattened = Object.entries(errors).map(([key, error]) => `${key}: ${error}`);
    super('Содержимое документа некорректно', flattened);
    this.name = 'AqsiOnlineValidationError';
    Error.captureStackTrace(this, AqsiOnlineValidationError);
  }
}

class AqsiOnlineApiError extends AqsiOnlineError {
  constructor(reason) {
    super(API_ERRORS[reason.statusCode] || reason.message, reason.error && reason.error.errors);
    this.name = 'AqsiOnlineApiError';

    const { request: req, ...res } = reason.response.toJSON();
    this.sentData = reason.options.body;
    this.request = req;
    this.response = res;
    Error.captureStackTrace(this, AqsiOnlineApiError);
  }
}

module.exports.AqsiOnlineError = AqsiOnlineError;
module.exports.AqsiOnlineValidationError = AqsiOnlineValidationError;
module.exports.AqsiOnlineApiError = AqsiOnlineApiError;
