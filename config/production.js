// Production specific configuration
// =================================
const production = {
  host: process.env.OD_HOST || 'api.aqsi.online',
  port: process.env.OD_PORT || 12004,
  baseUrl: process.env.OD_BASE_URL || 'api/v2',
};

module.exports = production;
