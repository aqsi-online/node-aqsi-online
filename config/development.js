// Development specific configuration
// ==================================
const development = {
  host: process.env.OD_HOST || 'api.aqsi.online',
  port: process.env.OD_PORT || 12021,
  baseUrl: process.env.OD_BASE_URL || 'api/v2',
};

module.exports = development;
